import os
import sys
from io import StringIO
from pathlib import Path

import minicli
from usine import run, sudo, connect, mkdir, chown, exists, put


def put_dir(local, remote):
    local = Path(local)
    remote = Path(remote)
    for path in local.rglob('*'):
        relative_path = path.relative_to(local)
        if path.is_dir():
            with sudo(user='hildevert'):
                mkdir(remote / relative_path)
        else:
            put(str(path), str(remote / relative_path))


def python(command):
    with sudo(user='hildevert'):
        run('/srv/hildevert/venv/bin/python {}'.format(command))


@minicli.cli
def pip(command):
    with sudo(user='hildevert'):
        run('/srv/hildevert/venv/bin/pip {}'.format(command))


@minicli.cli
def system():
    with sudo(set_home=False, login=False):
        run('apt update')
        run('apt install -y nginx postgresql gcc postgis '
            'postgresql-server-dev-all git software-properties-common '
            'python-virtualenv')
        run('add-apt-repository --yes --update ppa:jonathonf/python-3.6')
        run('apt-get install -y python3.6 python3.6-dev')
        mkdir('/srv/hildevert/data')
        run('useradd -N hildevert -d /srv/hildevert/ || exit 0')
        chown('hildevert:users', '/srv/hildevert/')
        run('chsh -s /bin/bash hildevert')


@minicli.cli
def db():
    with sudo(user='postgres'):
        run('createuser hildevert || exit 0')
        run('createdb hildevert -O hildevert || exit 0')
        run('psql hildevert -c "CREATE EXTENSION IF NOT EXISTS postgis"')
        run('psql hildevert -c "CREATE EXTENSION IF NOT EXISTS unaccent"')


@minicli.cli
def venv():
    path = '/srv/hildevert/venv/'
    if not exists(path):
        with sudo(user='hildevert'):
            run(f'virtualenv {path} --python=python3.6')
    pip('install pip -U')


@minicli.cli
def http():
    put('fabfile/nginx-http.conf', '/etc/nginx/sites-enabled/hildevert')
    restart()


@minicli.cli
def bootstrap():
    system()
    db()
    venv()
    service()
    http()


@minicli.cli
def cli(cmd):
    with sudo(user='hildevert'):
        run(f'/srv/hildevert/venv/bin/hildevert {cmd}')


@minicli.cli
def service():
    put('fabfile/hildevert.service', '/etc/systemd/system/hildevert.service')
    systemctl('enable hildevert.service')


@minicli.cli
def deploy():
    put('fabfile/gunicorn.conf', '/srv/hildevert/gunicorn.conf')
    pip('install -U git+https://framagit.org/ybon/hildevert')
    pip('install gunicorn')
    cli('initdb')
    restart()


@minicli.cli
def push_data():
    put_dir('data/', '/srv/hildevert/data/')


@minicli.cli
def import_data():
    cli('load /srv/hildevert/data/*.TXT')


@minicli.cli
def restart():
    with sudo():
        systemctl('restart hildevert nginx')


@minicli.cli
def systemctl(cmd):
    run(f'systemctl {cmd}')


@minicli.cli
def logs(lines=50):
    run('journalctl --lines {} --unit hildevert'.format(lines))


@minicli.cli
def status():
    systemctl('status nginx hildevert')


@minicli.cli
def psql(query):
    with sudo(user='postgres'):
        run(f'psql hildevert -c "{query}"')


@minicli.cli
def upload_environ():
    keys = ['HILDEVERT_USER', 'HILDEVERT_PWD']
    content = ''
    for key in keys:
        try:
            content += '{}={}\n'.format(key, os.environ[key])
        except KeyError as e:
            sys.exit('The {} environment variable does not exist.'.format(e))
    run('cat /srv/hildevert/private.env')
    put(StringIO(content), '/srv/hildevert/private.env')


if __name__ == '__main__':
    with connect('root@51.15.221.175'):
        minicli.run()

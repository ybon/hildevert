import asyncio
import csv
import re
import sys
from base64 import b64decode
from contextlib import suppress
from datetime import datetime
from http import HTTPStatus
from pathlib import Path

import asyncpg
import ujson as json
import uvloop
from asyncpg import create_pool
from jinja2 import Environment, PackageLoader, select_autoescape
from minicli import cli, run
from roll import HttpError, Query, Roll
from roll.extensions import cors, logger, simple_server, static, traceback

from .config import DB_CONFIG

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


class Query(Query):

    def year(self, key, default=...):
        value = self.get(key, default)
        if value:
            try:
                value = datetime.strptime(value, '%Y')
            except ValueError:
                raise HttpError(HTTPStatus.BAD_REQUEST,
                                f'`{value}` is not a valid year')
        return value


class Roll(Roll):
    Query = Query


app = Roll()
logger(app)
traceback(app)
cors(app)
static(app, root=Path(__file__).parent / 'static')

templates = Environment(
    loader=PackageLoader('hildevert', 'templates'),
    autoescape=select_autoescape(['html'])
)
COMMUNES = []


class Deed(dict):

    PRIVATE = ['acte', 'version', 'code_commune']

    def __init__(self, id, data, kind, place, dept_id, **kwargs):
        self.id = id
        self.kind = kind
        self.place = place
        self.dept_id = dept_id
        super().__init__(data)

    @staticmethod
    def load(**kwargs):
        if kwargs['kind'] == 'M':
            return Wedding(**kwargs)
        elif kwargs['kind'] == 'D':
            return Death(**kwargs)
        else:
            return Baptism(**kwargs)

    def __iter__(self):
        for key in self.COLUMNS:
            if key not in self.PRIVATE and self[key].strip():
                yield key, self[key]


class Baptism(Deed):

    KIND = 'Naissance'
    COLUMNS = [
        'version', 'code_commune', 'commune', 'code_département',
        'département', 'acte', 'date', 'modified_at', 'modified_by', 'X1',
        'nom', 'prénoms', 'sexe', 'commentaire', 'nom_du_père',
        'prénom_du_père', 'commentaire_sur_le_père', 'profession_père',
        'nom_de_la_mère', 'prénoms_de_la_mère', 'commentaire_sur_la_mère',
        'profession_de_la_mère', 'nom_du_parrain', 'prénoms_du_parrain',
        'commentaire_sur_le_parrain', 'nom_de_la_marraine',
        'prénoms_de_la_marraine', 'commentaire_sur_la_marraine',
        'source'
    ]

    def __str__(self):
        return '{prénoms} {nom}'.format(**self)

    async def insert(conn, data):
        tsv_A = [data['nom']]
        tsv_C = [data['prénoms']]
        tsv_D = [data['nom_de_la_mère']]
        if data['sexe']:
            data['sexe'] = 'féminin' if data['sexe'] == 'F' else 'masculin'
        await insert_deed(conn, tsv_A, tsv_C, tsv_D, data)


class Wedding(Deed):

    KIND = 'Mariage'
    COLUMNS = [
        'version', 'code_commune', 'commune', 'code_département',
        'département', 'acte', 'date', 'modified_at', 'modified_by', 'X1',
        'nom_époux', 'prénoms_époux', 'origine_époux', 'naissance_époux',
        'âge_époux', 'commentaire_époux', 'profession_époux',
        'nom_ex conjoint epx', 'prénoms_ex-cjt époux',
        'commentaire_ex_cjt_épx', "nom_père_époux", 'prénoms_père_époux',
        'commentaire_père_épx', 'profession_père_époux', "nom_mere époux",
        "prénoms_mère_époux", 'commentaire_mère_époux',
        'profession_mère_époux', 'nom_épouse', 'prénoms_épouse',
        'origine_épouse', 'naissance_épouse', 'âge_épouse',
        'commentaire_épouse', 'profession_épouse', 'nom_ex_conjoint_épse',
        'prénoms_ex-cjt épouse', 'commentaire_ex_cjt_épse', "nom_père_épouse",
        'prénoms_père_épouse', 'commentaire_père_épse',
        'profession_père_épouse', "nom_mère_épouse", "prénoms_mère_épouse",
        'commentaire_mère_épouse', 'profession_mère_épouse', 'nom_témoin 1',
        'prénoms_témoin 1', 'commentaire_témoin 1', 'nom_témoin 2',
        'prénoms_témoin 2', 'commentaire_témoin 2', 'nom_témoin 3',
        'prénoms_témoin 3', 'commentaire_témoin 3', 'nom_témoin 4',
        'prénoms_témoin 4', 'commentaire_témoin 4', 'source'
    ]

    def __str__(self):
        return ('{prénoms_époux} {nom_époux} et {prénoms_épouse} '
                '{nom_épouse}'.format(**self))

    async def insert(conn, data):
        tsv_A = [data['nom_époux'], data['nom_épouse']]
        tsv_C = [data['prénoms_époux'], data['prénoms_épouse']]
        tsv_D = [data["nom_mère_épouse"]]
        await insert_deed(conn, tsv_A, tsv_C, tsv_D, data)


class Death(Deed):

    KIND = 'Décès'
    COLUMNS = [
        'version', 'code_commune', 'commune', 'code_département',
        'département', 'acte', 'date', 'modified_at', 'modified_by', 'X1',
        'nom', 'prénoms', 'origine', 'date_de_naissance', 'sexe', 'âge',
        'commentaire', 'profession', 'nom_conjoint', 'prénom_conjoint',
        'commentaire_conjoint', 'profession_conjoint', 'nom_du_père',
        'prénoms_du_père', 'commentaire_sur_le_père', 'profession_du_père',
        'nom_de_la_mère', 'prénoms_de_la_mère', 'commentaire_sur_la_mère',
        'profession_de_la_mère', 'nom_temoin_1', 'prénoms_témoin_1',
        'commentaire_témoin_1', 'nom_temoin_2', 'prénoms_témoin_2',
        'commentaire_témoin_2', 'source'
    ]

    def __str__(self):
        return '{prénoms} {nom}'.format(**self)

    async def insert(conn, data):
        tsv_A = [data['nom']]
        tsv_C = [data['prénoms']]
        tsv_D = [data['nom_de_la_mère'], data['nom_conjoint']]
        if data['sexe']:
            data['sexe'] = 'féminin' if data['sexe'] == 'F' else 'masculin'
        await insert_deed(conn, tsv_A, tsv_C, tsv_D, data)


def authenticate(func):

    async def wrapper(request, response, *args, **kwargs):
        auth = request.headers.get('AUTHORIZATION', '')
        if b64decode(auth[6:]) != b'agpb:AGPB':  # Make me sikret.
            response.status = HTTPStatus.UNAUTHORIZED
            response.headers['WWW-Authenticate'] = 'Basic'
        else:
            await func(request, response, *args, **kwargs)

    return wrapper


async def register(conn):
    def _encoder(value):
        return json.dumps(value)

    def _decoder(value):
        return json.loads(value)

    await conn.set_type_codec(
        'jsonb', encoder=_encoder, decoder=_decoder,
        schema='pg_catalog'
    )

    results = await conn.fetch('SELECT DISTINCT(place) '
                               'FROM deed ORDER BY place')
    global COMMUNES
    COMMUNES = [r['place'] for r in results]


async def select(request):
    where = []
    args = []
    from_ = ''
    select = []
    order_by = []
    q = request.query.get('q', None)
    if q:
        if q.endswith('*') and not q.endswith(':*'):
            q = q.replace('*', ':*')
        pattern = re.compile('(\w) +(\w)')
        q = pattern.sub(r'\1 & \2', q)
        from_ = ", to_tsquery('fts', {}) AS query"
        where.append("tsv @@ query")
        select.append('ts_rank_cd(tsv, query) AS rank')
        order_by.append('rank DESC')
        args.append(q)
    else:
        select.append('1 AS rank')
    from_year = request.query.year('from_year', None)
    if from_year:
        where.append('date >= {}')
        args.append(from_year)
    to_year = request.query.year('to_year', None)
    if to_year:
        where.append('date <= {}')
        args.append(to_year)
    kind = request.query.get('kind', None)
    if kind:
        where.append('kind = {}')
        args.append(kind)
    place = request.query.get('place', None)
    if place:
        where.append('place = {}')
        args.append(place)
    order_by.append('date')
    where = where and 'WHERE {}'.format(' AND '.join(where)) or ''
    select = select and ', {}'.format(', '.join(select)) or ''
    order_by = order_by and 'ORDER BY {}'.format(', '.join(order_by)) or ''
    query = f'''
        SELECT
            id, kind, date, place, dept_id, dept_name, data{select}
        FROM
            deed {from_}
        {where}
        {order_by}
        LIMIT
            50
    '''
    # Make {} placeholders to $x.
    query = query.format(*['$' + str(i) for i in range(1, len(args) + 1)])
    async with app.pool.acquire() as conn:
        return [[Deed.load(**r), r['rank']]
                for r in await conn.fetch(query, *args)]


@app.route('/')
@authenticate
async def home(request, response):
    deeds = []
    if request.query:
        deeds = await select(request)
        ids = '|'.join([str(d.id) for d, r in deeds])
        response.cookies.set('results', ids)
        response.cookies.set('search', request.url.decode())
    template = templates.get_template('deeds.html')
    response.body = template.render(deeds=deeds, request=request,
                                    COMMUNES=COMMUNES)
    response.headers['Content-Type'] = 'text/html; charset=utf-8'


@app.route('/deed/{id:digit}')
@authenticate
async def deed(request, response, id):
    try:
        id = int(id)
    except (ValueError, TypeError):
        raise HttpError(HTTPStatus.BAD_REQUEST, f'`{id}` is not a valid id')
    async with app.pool.acquire() as conn:
        data = await conn.fetchrow('SELECT * FROM deed WHERE id=$1', id)
    if not data:
        raise HttpError(HTTPStatus.NOT_FOUND, f'{id} was not found')
    deed = Deed.load(**data)
    template = templates.get_template('deed.html')
    next, previous, search = None, None, None
    if 'results' in request.cookies:
        try:
            ids = list(map(int, request.cookies['results'].split('|')))
        except (ValueError, TypeError):
            pass
        else:
            current = ids.index(id)
            if current > 0:
                previous = ids[current - 1]
            if 0 <= current < len(ids) - 1:
                next = ids[current + 1]
    if 'search' in request.cookies:
        search = request.cookies['search']
    response.body = template.render(deed=deed, request=request, next=next,
                                    previous=previous, search=search)
    response.headers['Content-Type'] = 'text/html; charset=utf-8'


@app.listen('startup')
async def register_db():
    app.pool = await create_pool(**DB_CONFIG, max_size=100, loop=app.loop,
                                 init=register)


@cli
def serve():
    simple_server(app)


@cli
async def initdb():
    """Initialize DB tables."""
    conn = await asyncpg.connect(**DB_CONFIG)
    async with conn.transaction():
        await conn.execute('CREATE EXTENSION IF NOT EXISTS unaccent')
        # Drop and recreate, IF NOT EXISTS cannot be used with CREATE TEXT
        # SEARCH CONFIGURATION
        await conn.execute('DROP TEXT SEARCH CONFIGURATION IF EXISTS fts')
        await conn.execute('CREATE TEXT SEARCH CONFIGURATION '
                           'fts (COPY=simple)')
        await conn.execute('ALTER TEXT SEARCH CONFIGURATION fts ALTER MAPPING '
                           'FOR word WITH unaccent, simple')
        await conn.execute(
            '''
            CREATE TABLE IF NOT EXISTS deed (
                id SERIAL PRIMARY KEY,
                kind VARCHAR(1) NOT NULL,
                date DATE NOT NULL,
                place VARCHAR(256) NOT NULL,
                dept_id smallint NOT NULL,
                dept_name VARCHAR(256) NOT NULL,
                tsv TSVECTOR NOT NULL,
                data JSONB NOT NULL
            )
            ''')
        await conn.execute('CREATE INDEX IF NOT EXISTS tsv_idx '
                           'ON deed USING gin(tsv)')
    print('DB initialized')


def parse_date(value):
    with suppress(ValueError):
        return datetime.strptime(value, '%d/%m/%Y')

    # Consider the day was invalid, try with month only.
    with suppress(ValueError):
        return datetime.strptime(value[3:], '%m/%Y')

    # Consider even the month was invalid, try with year only.
    # But let's raise this time, row will be logged and skipped.
    return datetime.strptime(value[-4:], '%Y')


async def insert_deed(conn, tsv_A, tsv_C, tsv_D, data):
    del data['version']
    await conn.execute(
        'INSERT INTO deed (kind, date, place, dept_id, dept_name, tsv, data) '
        'VALUES($1, $2, $3, $4, $5, '
        "setweight(to_tsvector('fts', $6), 'A') || "
        "setweight(to_tsvector('fts', $7), 'C') || "
        "setweight(to_tsvector('fts', $8), 'D'), $9)",
        data['acte'], parse_date(data['date']),
        data['commune'], int(data['code_département']), data['département'],
        ' '.join(tsv_A), ' '.join(tsv_C), ' '.join(tsv_D), data)


@cli
async def load(*paths: Path):
    conn = await asyncpg.connect(**DB_CONFIG)
    await register(conn)
    for path in paths:
        if not path.exists():
            sys.exit(f'File "{path}"" does not exist')
        with path.open(encoding='latin1') as f:
            for line in csv.reader(f, delimiter=';'):
                if line[5] == 'N':
                    klass = Baptism
                elif line[5] == 'M':
                    klass = Wedding
                elif line[5] == 'D':
                    klass = Death
                else:
                    print('Unknown type', line)
                    continue
                try:
                    data = dict(zip(klass.COLUMNS, line))
                    await klass.insert(conn, data)
                except Exception as e:
                    print('-' * 80)
                    print(e)
                    print(path)
                    print(line)
                    print(data)


if __name__ == '__main__':
    run()

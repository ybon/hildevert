import logging
import os


LOGGING_LEVEL = logging.DEBUG
DB_CONFIG = {
    'database': os.environ.get('POSTGRES_DB', 'hildevert'),
    'host': os.environ.get('POSTGRES_HOST', None),
    'user': os.environ.get('POSTGRES_USER', None),
    'password': os.environ.get('POSTGRES_PASSWORD', None),
}
